% Conception d'un distribuable écrit en Python
%
% Auteur  : Gregory DAVID
%%

\usepackage{style/glossaire}
\usepackage{bashful}

\addbibresource{livrable-python.bib}

\newcommand{\MONTITRE}{Concevoir un distribuable (\emph{release})}
\newcommand{\MONSOUSTITRE}{en Python3 avec \texttt{setuptools} et \texttt{stdeb}}
\newcommand{\DISCIPLINE}{\Gls{SLAMcinq}}%
\newcommand{\GREGORY}{Grégory~DAVID}%

\title{%
  \begin{flushright}
      \noindent{\Huge {\bf \MONTITRE}} \newline
      \noindent{\huge \MONSOUSTITRE} \newline
      \noindent{\large \DISCIPLINE} \newline
  \end{flushright}
}

\mode<presentation>{%
  \title{\MONTITRE}
  \subtitle{\MONSOUSTITRE}
  \author[version en date du \today, \GREGORY]{\GREGORY}
  \date{\today}
  \institute{\DISCIPLINE}
}%

\begin{document}
\mode* % empêche la création de slide hors \frame

\mode<article>{ \pagestyle{empty} }

\begin{frame}
    \maketitle
\end{frame}
\mode<article>{ \vspace{\fill} }
\begin{frame}
    \tableofcontents
\end{frame}

\mode<article>{ \newpage \pagestyle{fancy} }

\section{Programme initial}
On considèrera que le projet s'appelle \texttt{programme}. Par
conséquent, partout où vous trouverez le terme \texttt{programme},
c'est que le nom de notre projet est employé.

\subsection{Structure et contenu}

% Clean
\bash
rm -rfv programme/{deb_dist,dist,programme.egg-info,*.tar.gz}
\END

\bash
cd programme
touch programme/main.py
tree programme --noreport --prune -F -n -P '*.py'
rm programme/main.py
\END

\lstset{caption={Structure initiale du projet},language={}}
\lstinputlisting{\jobname.stdout}

\begin{frame}
\lstset{language={Python}}%
\lstinputlisting[%
caption={Contenu du fichier
  \texttt{programme/\_\_init\_\_.py}}]{programme/programme/__init__.py.std}
\end{frame}

\begin{frame}
\lstset{language={Python}}%
\lstinputlisting[%
caption={Contenu du fichier
  \texttt{programme/main.py}}]{programme/programme/main.py.std}
\end{frame}

\begin{frame}
\lstset{language={Python}}%
\lstinputlisting[%
caption={Contenu du fichier
  \texttt{programme/mod.py}}]{programme/programme/mod.py.std}
\end{frame}

\begin{frame}
\lstset{language={Python}}%
\lstinputlisting[%
caption={Contenu du fichier
  \texttt{programme/ressources/x.py}}]{programme/programme/ressources/x.py}
\end{frame}

\subsection{Invocation directe du module principal}
\begin{lstlisting}[caption={Appel direct du programme principal, situé
dans le module \texttt{main.py}, avec l'interpréteur Python3},
language={}]
> $ cd programme
> $ python3 main.py
0.7367114061522636
86.69636778252382
\end{lstlisting}

\clearpage
\section[Programme paquet]{Programme en tant que paquet Python3}
\begin{frame}
L'idée derrière la notion de \textbf{programme paquet} est de
permettre d'avoir un programme disponible depuis l'appel d'un paquet
et non plus depuis l'appel d'un module en particulier.
\end{frame}

Dans ce cadre, nous pourrons alors envisager de créer un lanceur
global faisant l'appel au programme paquet.

\subsection{Structure et contenu}
Pour rendre le paquet exécutable en tant que \textbf{programme
  paquet}, il est nécessaire de renommer le module principal en le
fichier d'initialisation du paquet \texttt{\_\_init\_\_.py} :\\
\texttt{mv main.py \_\_init\_\_.py}

\bash
tree programme --noreport --prune -F -n -P '*.py|runner' -I 'setup.py'
\END

\lstset{caption={Structure du projet dans le cadre du programme paquet}}
\lstinputlisting{\jobname.stdout}

\lstinputlisting[%
language={Python},%
caption={Contenu du fichier \texttt{runner}, devanant alors le lanceur
  du programme en mode développement}]{programme/runner}

\bash
diff -u programme/programme/__init__.py.std programme/programme/__init__.py
\END

\lstset{language={diff}}%
\lstinputlisting[%
caption={Contenu modifié du fichier
  \texttt{programme/\_\_init\_\_.py}, issu du fichier
  \texttt{programme/main.py}}]{\jobname.stdout}%

\bash
diff -u programme/programme/mod.py.std programme/programme/mod.py
\END

\lstinputlisting[%
caption={Contenu modifié du fichier \texttt{programme/mod.py}},
language={diff}]{\jobname.stdout}

\subsection{Invocation du programme}
\subsubsection{Par chargement du module principal}
\begin{lstlisting}[caption={Appel du programme par la voie du package
avec l'interpréteur Python3}, language={}]
> $ python3 -m programme.__init__
0.21081097151185746
6.257391276842805
\end{lstlisting}

\subsubsection{Par utilisation du lanceur global}
\begin{lstlisting}[caption={Appel du programme par la voie du lanceur
\texttt{runner}}, language={}]
> $ ./runner
0.21081097151185746
6.257391276842805
\end{lstlisting}

\clearpage
\section{Les setuptools}
\subsection{Pré-requis logiciels et installation}
\begin{lstlisting}[caption={Installation, pour l'ensemble du système,
des paquets Debian nécessaires à l'usage des
\texttt{setuptools}. Nécessite les droits du super utilisateur
\texttt{root}}, language={}]
> $ apt install python3-setuptools
\end{lstlisting}

\subsection{Configuration}
La configuration des \texttt{setuptools} nécessite de modifier quelque
peu la structure de notre projet comme suit.

\bash
tree programme --noreport --prune -F -n -P '*.py|runner'
\END

\lstinputlisting[%
caption={Structure du projet avec les \texttt{setuptools}},
language={}]{\jobname.stdout}

La configuration des \texttt{setuptools} se fait donc dans le fichier
\texttt{programme/setup.py}.

\lstset{language={Python}}%
\lstinputlisting[%
basicstyle={\ttfamily\footnotesize},%
caption={Contenu du fichier
  \texttt{programme/setup.py}}]{programme/setup.py}

Vous noterez que la ligne 21 présente les bibliothèques dont notre
projet dépend.

\subsection{Création du paquet source distribuable}
\bash[ignoreStderr=true,prefix={}]
cd programme
python3 setup.py sdist
\END

\lstset{language={},caption={Exécution de la création du paquet source
    distribuable avec \texttt{setuptools}}}
\lstinputlisting{\jobname.sh}%

\lstset{language={},caption={Sortie de la création du paquet source
    distribuable avec \texttt{setuptools}}}
\lstinputlisting[basicstyle={\ttfamily\footnotesize}]{\jobname.stdout}%

\bash
tree programme --noreport --prune -F -n -P '*.py|runner|*.tar*'
\END

\lstset{caption={Structure du projet après construction du paquet
    source distribuable}, language={}}%
\lstinputlisting{\jobname.stdout}

\subsection{Installation depuis le paquet source distribué}
Considérons que le paquet source que l'on vient de créer a été
distribué à un utilisateur (par téléchargement sur un site web par
exemple).

Dans ce cas un utilisateur devra réaliser l'installation comme suit :

\bash[ignoreStderr,ignoreExitCode,prefix={}]
pip3 install programme/dist/programme-0.4.2.tar.gz
\END

\lstset{caption={Procédure d'installation depuis le paquet source par
    l'utilisateur}, language={}}%
\lstinputlisting{\jobname.sh}

Suite à quoi, l'utilisateur final pourra lancer depuis un terminal
l'exécutable :%

\lstset{caption={Exécution par l'utilisateur final du programme
  nouvellement installé}}%

\bash[script,stdout,ignoreStderr,ignoreExitCode,prefix={}]
export PATH=$HOME/.local/bin:$PATH
programme-executable
\END

Par ailleurs, comme le programme est installé en tant que package de
l'environnement Python3, il est aussi possible de charger le module
depuis la console Python3 ou depuis n'importe quel autre programme
écrit en Python3.

\begin{lstlisting}[caption={Exploitation du package depuis la console
Python3}, language={}]
> $ python3
Python 3.5.2+ (default, Sep 22 2016, 12:18:14) 
[GCC 6.2.0 20160927] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import programme
>>> programme.run()
0.47533918198963276
29.810847195879376
>>>
\end{lstlisting}

\subsection{Suppression du paquet installé}
Une fois que le paquet a été installé avec \texttt{pip3}, la commande
suivante va permettre d'en effectuer la suppression -- en spécifiant le
numéro de la version du paquet à supprimer dans le cas où plusieurs
versions pourraient cohabiter.

\lstset{caption={Procédure de suppression avec les droits du super
    utilisateur}, language={}}%

\bash[script,stdout,ignoreStderr,ignoreExitCode,prefix={}]
pip3 uninstall --yes programme==0.4.2
\END


\clearpage
\section[Packaging Debian avec stdeb]{Génération de paquet Debian avec stdeb}
\subsection{Pré-requis logiciels et installation}
\begin{lstlisting}[caption={Installation, pour l'ensemble du système,
des paquets Debian nécessaires à l'usage de \texttt{stdeb}. Nécessite
les droits du super utilisateur \texttt{root}}, language={}]
> # apt install devscripts fakeroot python3-all apt-file python3-stdeb
\end{lstlisting}

\subsection{Configuration}
La configuration de \texttt{stdeb} n'est pas obligatoire car elle
repose tout d'abord sur la configuration des \texttt{setuptools} dans
\texttt{programme/setup.py}.

Cependant, la configuration spécifique pour le paquet Debian résultant
peut se faire dans le fichier \texttt{programme/stdeb.cfg}. Les
éléments de configuration sont listés dans la documentation officielle
de \texttt{stdeb} :
\url{https://pypi.python.org/pypi/stdeb/#stdeb-cfg-configuration-file}.

\lstset{caption={Structure du projet avant la création du paquet
    Debian, avec la configuration suplémentaire de \texttt{stdeb}},
  language={}}

\bash[stdout,ignoreStderr,ignoreExitCode]
tree programme --noreport --prune -F -n -P '*.py|runner|*.cfg'
\END

La configuration de \texttt{stdeb} se fait donc dans le fichier
\texttt{programme/stdeb.cfg}, dans lequel nous allons spécifier les
paquets Debian dont dépendra notre propre paquet.

Cette définition de dépendance est très intéressante car elle empĉhe
toute installation de notre paquet sur un système pour lequel la
dépendance n'est pas satisfaite.

Ainsi, notre paquet ne viendra pas polluer le système d'un utilisateur
où les dépendance sont manquantes.

\lstinputlisting[caption={Contenu du fichier de configuration de
  \texttt{stdeb} pour le paquet logiciel \texttt{programme} :
  \texttt{programme/stdeb.cfg}}, language={}]{programme/stdeb.cfg}

\subsection{Création du paquet binaire Debian distribuable}
\bash[prefix={},ignoreStderr,ignoreExitCode]
cd programme
python3 setup.py --command-package=stdeb.command bdist_deb
\END

\lstset{%
  basicstyle={\ttfamily\footnotesize},%
  caption={Exécution de la création du paquet binaire Debian
    distribuable avec \texttt{stdeb}}, language={}}%
\lstinputlisting{\jobname.sh}

\bash
tree programme --noreport --prune -F -n -I '*.std|*~|*.gin|*.pyc'
\END

\lstset{%
  basicstyle={\ttfamily\footnotesize},%
  caption={Structure du projet après construction du paquet binaire
    distribuable. Nous pouvons remarquer la spécificité Debian dans :\\
    \texttt{programme/deb\_dist/programme-0.4.2/debian}},
  language={}}%
\lstinputlisting{\jobname.stdout}

\subsection{Installation du paquet binaire Debian distribué}
\begin{lstlisting}[caption={Installation, par le super utilisateur, du
    paquet Debian distribué. S'il manque une dépendance, alors le
    paquet ne sera pas installé par \texttt{dpkg} et une information
    sur les paquets manquants sera affichée. À charge du super
    utilisateur de résoudre ce problème}]
> # dpkg -i python3-programme_0.4.2-1_all.deb
\end{lstlisting}

\subsection{Suppression du paquet binaire Debian distribué}
\begin{lstlisting}[caption={Suppression, par le super utilisateur, du
    paquet Debian distribué}]
> # apt remove python3-programme
\end{lstlisting}

\clearpage

\appendix%
\part*{Annexes}
\section{Virtualisation avec KVM}
KVM (\emph{Kernel-based Virtual Machine}) est un hyperviseur libre de
type I pour Linux. KVM est intégré dans le noyau Linux depuis la
version \textbf{2.6.20}.

Il fonctionne originellement sur les processeurs à architectures
\texttt{x86} disposant des instructions de virtualisation \emph{Intel
  VT} ou \emph{AMD-V}. Depuis, KVM a été porté pour les architectures
\emph{Power PC}, \emph{IA-64} ainsi que \emph{ARM} depuis le noyau Linux
\textbf{3.9}.

\subsection{Installation}
Dans un environnement nativement Debian, ou les distributions
dérivées, il suffit d'installer les paquets adaptés :
\begin{lstlisting}[caption={Installation, pour l'ensemble du système,
des paquets Debian nécessaires à l'usage de \texttt{kvm}. Nécessite
les droits du super utilisateur \texttt{root}}, language={}]
> # apt install qemu-kvm
\end{lstlisting}

\subsection{Vérification}
Lors de l'installation, le module \texttt{kvm} doit être chargé dans
le noyau. Nous devons donc nous en assurer:
\begin{lstlisting}[caption={Vérification du chargement du module noyau
\texttt{kvm}}, language={}]
> $ lsmod | grep kvm
kvm_intel             188416  0
kvm                   585728  1 kvm_intel
irqbypass              16384  1 kvm
\end{lstlisting}

Si votre architecture matérielle est basée sur du processeur
\emph{AMD}, alors le module noyau qui est chargé se nomme
\texttt{kvm\_amd} en lieu et place du module \texttt{kvm\_intel}.

\subsection{Lancement d'une machine virtuelle}
Nous considèrerons que vous avez à disposition une image disque
contenant un système d'exploitation déjà installé et configuré (vous
pourrez prendre l'image disque \texttt{liveUSB} disponible sur
\url{http://liveusb.groolot.net/?dir=sio/gnome}).

\begin{lstlisting}[caption={Invocation de \texttt{kvm} pour démarrer
une machine virtuelle existante. Le disque virtuel est stocké dans le
fichier \texttt{liveUSB.img} et la mémoire du système virtuel défini à
$1024 Mo$}, language={}]
> $ kvm -hda liveUSB.img -m 1024
\end{lstlisting}

\paragraph{Note} \texttt{kvm} est un raccourci vers l'application
\texttt{qemu-system-x86\_64 -enable-kvm} convenablement paramétrée
pour un usage dans l'espace noyau.

\paragraph{Documentation} Vous pourrez consulter la page de manuel de
\texttt{kvm} avec \texttt{man kvm}, ainsi que la page de manuel de
\texttt{qemu-system} pour connaître toutes les options de
configuration d'une machine virtuelle possibles.

\clearpage

\lstlistoflistings

% \begin{frame}
%     \printbibheading
%     \printbibliography[nottype=online,check=notonline,heading=subbibliography,title={Bibliographiques}]
%     \printbibliography[check=online,heading=subbibliography,title={Webographiques}]
% \end{frame}
% \newpage
% \printglossaries
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "support"
%%% End:
