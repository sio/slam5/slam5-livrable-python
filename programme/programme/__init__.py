from .mod import start
import lxml

def run():
    start()

if __name__ == "__main__":
    run()
